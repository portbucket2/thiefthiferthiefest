﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class FollowCameraTopDown : MonoBehaviour
{
    [SerializeField] Transform playerTransform;
    [SerializeField] bool isFollowing = true;
    [SerializeField] Vector3 offset = new Vector3(0f, 80f, -40f);

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (isFollowing)
        {
            transform.DOLocalMove(playerTransform.position + offset, 0.1f).SetEase(Ease.Flash);
        }
    }
}
