﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleLaserMatch : PuzzleGroup
{
    [SerializeField] PuzzleManager puzzleManager;
    [SerializeField] bool isRunning = true;
    [SerializeField] Camera puzzleCamera;
    [SerializeField] LineRenderer ln;
    [SerializeField] GameObject ghost;
    [SerializeField] ParticleSystem ghostEndParticle;
    [SerializeField] LaserRotationObjects[] reflectors;

    int lnIndex = 0;
    public Vector3[] hitPoints;
    public List<Transform> floaters;
    Vector3[] startRotaions;
    private void Awake()
    {
        InitRotations();
    }
    void Start()
    {
        puzzleManager = transform.GetComponentInParent<PuzzleManager>();
        puzzleCamera = puzzleManager.puzzleCamera;

        SetInitRotations();
        ghost.SetActive(true);
        CastRay(reflectors[0].transform, reflectors[1].transform);
        //hitPoints = new Vector3[reflectors.Length + 1];
        hitPoints[0] = reflectors[0].transform.position;
        ln.positionCount = reflectors.Length + 1;
        ln.SetPosition(0, reflectors[0].transform.position);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (isRunning)
            {
                Ray ray = puzzleCamera.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;

                if (Physics.Raycast(ray, out hit, 100))
                {
                    if (hit.transform.CompareTag("WireHead"))
                    {                        
                        //Debug.DrawLine(ray.origin, hit.point, Color.green, 1f);
                        hit.transform.GetComponent<LaserRotationObjects>().RotateOnce();                        
                    }

                }

            }
        }
        if (Input.GetMouseButtonUp(0))
        {
            CastRay(reflectors[0].transform, reflectors[1].transform);
        }
        //CastRay(reflectors[0].transform, reflectors[1].transform);
    }

    void CastRay(Transform t1,Transform t2)
    {
        //Debug.Log("laser on");
        hitPoints = new Vector3[4];
        hitPoints[0] = reflectors[0].transform.position;
        lnIndex = 0;
        Vector3 dir = t2.position - t1.position;
        Ray ray = new Ray(t1.position, dir);
        RaycastHit hit;
        //Debug.DrawRay(ray.origin, dir, Color.white, 10f);
        if (Physics.Raycast(ray, out hit, 100))
        {
            if (hit.transform.CompareTag("WireHead"))
            {
                //Debug.Log("hit 1");
                //Debug.DrawLine(ray.origin, hit.point, Color.red, 100f);
                //Debug.DrawRay(hit.point, hit.normal , Color.white, 100f);
                //AddLine(hit.point);
                Reflect(hit.point, dir, hit.normal);
            }else if (hit.transform.CompareTag("WireEnd"))
            {
                Debug.Log("Ghost Hit!!");                
            }
        }
    }
    void Reflect(Vector3 position, Vector3 direction, Vector3 normal)
    {
        //Debug.Log("hit 2 start");
        direction = Vector3.Reflect(direction, normal);
        //Debug.DrawRay(position, direction, Color.green, 100f);
        AddLine(position);
        //Vector3 startingPosition = position;
        Ray ray = new Ray(position, direction);
        RaycastHit hit2;
        if (Physics.Raycast(ray, out hit2, 100f))
        {
            if (hit2.transform.CompareTag("WireHead"))
            {
                //Debug.Log("hit 2");
                //Debug.DrawLine(ray.origin, hit2.point, Color.red, 100f);
                //Debug.DrawRay(hit2.point, hit2.normal, Color.white, 100f);

                Reflect(hit2.point, direction, hit2.normal);
            }
            else if (hit2.transform.CompareTag("WireEnd"))
            {
                //Debug.Log("Ghost Hit!!");
                Reflect(hit2.point, direction, hit2.normal);
                PuzzleEnd();
            }            
        }
        else
        {
            //hitPoints[hitPoints.Length - 1] = position+ direction;
            for (int i = 0; i < hitPoints.Length; i++)
            {
                if (Vector3.Distance(hitPoints[i],Vector3.zero)<0.05f )
                {
                    hitPoints[i] = position + direction;
                }
                floaters[i].position = hitPoints[i];
            }
            ln.SetPositions(hitPoints);
        }
    }
    public Camera GetCamera()
    {
        return puzzleCamera;
    }
    void AddLine(Vector3 position)
    {
        lnIndex++;
        //Debug.Log("ln pos:_" + lnIndex + "_" + position);
        hitPoints[lnIndex] = position;
    }

    void PuzzleEnd()
    {
        StartCoroutine(PuzzleEndRoutine());
    }
    IEnumerator PuzzleEndRoutine()
    {
        ghostEndParticle.Play();
        yield return new WaitForSeconds(0.5f);
        ghost.SetActive(false);
        yield return new WaitForSeconds(0.5f);
        gameObject.SetActive(false);
        puzzleManager.PuzzleCompleted();
    }
    public void ResetPuzzle() {

    }

    void InitRotations()
    {
        int max = reflectors.Length;
        startRotaions = new Vector3[max];
        for (int i = 0; i < max; i++)
        {
            startRotaions[i] = reflectors[i].transform.localEulerAngles;
        }
    }
    void SetInitRotations()
    {
        int max = reflectors.Length;
        for (int i = 0; i < max; i++)
        {
            reflectors[i].transform.localEulerAngles = startRotaions[i]; 
        }
    }
}
