﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleManager : MonoBehaviour
{
    //[SerializeField] PuzzleColorMatchWire wirePuzzle;
    public Camera puzzleCamera;
    public ParticleSystem burstLight;

    [SerializeField] PuzzleGroup[] puzzleGroup;
    [Header("Auto Fill")]
    [SerializeField] int currentPuzzle = 0;
    [SerializeField] int totalPuzzles = 0;
    GameController gameController;
    bool isLeveComplete = false;
    void Start()
    {
        gameController = GameController.GetController();
        totalPuzzles = puzzleGroup.Length;
    }
    public void StartNextPuzzle()
    {
        for (int i = 0; i < totalPuzzles; i++)
        {
            puzzleGroup[i].gameObject.SetActive(false);
        }
        puzzleGroup[currentPuzzle].gameObject.SetActive(true);
        puzzleGroup[currentPuzzle].ResetPuzzle();
    }
    public void PuzzleCompleted()
    {
        SelectNextPuzzle();
        gameController.CompletePuzzle();
    }

    void SelectNextPuzzle()
    {

        if (totalPuzzles > 1) 
        {
            currentPuzzle++;
            if (currentPuzzle > totalPuzzles - 1)
            {
                currentPuzzle = 0;
                Debug.Log("level Complete");
                
            }
        }
        else
        {
            currentPuzzle = 0;
        }
        
    }
}
