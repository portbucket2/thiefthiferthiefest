﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class PlayerController : MonoBehaviour
{
    public StageManager stageManager;
    public Vector3 distaceFromgate;
    public Vector3 laneOffset;
    public Animator animator;
    public float walkTime = 1.2f;

    private List<Vector3> lane;
    private int gateCount = 0;
    GameController gameController;
    // Start is called before the first frame update

    bool isMoving = false;
    Vector3 finishLine;
    float totalDistance = 0;

    private readonly string IDLE = "idle";
    private readonly string WALK = "walk";
    private readonly string VICTORY = "victory";
    private readonly string LOSE = "lose";

    void Start()
    {
        gameController = GameController.GetController();   
    }

    // Update is called once per frame
    void Update()
    {
        if (isMoving)
        {
            UpdateProgress();
        }
        
    }
    public void InitPlayer(List<Vector3> _lane)
    {
        gateCount = 0;
        lane = new List<Vector3>();
        lane = _lane;
        finishLine = lane[lane.Count - 1];
        totalDistance = Vector3.Distance(transform.position, finishLine);
    }
    public void MoveToNext()
    {
        isMoving = true;
        if (gateCount<lane.Count-1)
        {
            transform.DOLocalMove((lane[gateCount] + laneOffset) - distaceFromgate, walkTime).SetEase(Ease.Linear).OnComplete(CallNextPuzzle);
        }
        else
        {
            Debug.Log("stage complete");
            //transform.DOLocalMove(transform.localPosition + distaceFromgate * 20f, walkTime).SetEase(Ease.Linear).OnComplete(StageComplete); 
            transform.DOLocalMove((lane[gateCount] + laneOffset), walkTime).SetEase(Ease.Linear).OnComplete(StageComplete); 
        }
        animator.SetTrigger(WALK);
    }
    void CallNextPuzzle()
    {
        isMoving = false;
        StopWalking();
        gateCount++;
        gameController.StartPuzzle();
    }
    public void OnPuzzleComplete()
    {
        //puzzle panel off
        // move to next gate
        MoveToNext();
    }
    void StageComplete()
    {
        StopWalking();
        //Dance();
        // start dancing
        // end stage
        gameController.LevelComplete();

    }
    void StopWalking()
    {
        animator.SetTrigger(IDLE);
    }
    public void Dance()
    {
        animator.SetTrigger(VICTORY);
    }
    public void Lose()
    {
        animator.SetTrigger(LOSE);
    }
    void UpdateProgress() {
        float currentDistance = Vector3.Distance(transform.position, finishLine);
        float val = 0;
        val = currentDistance / totalDistance;
        gameController.UpdateProgressBar(1f - val);
    }

}
