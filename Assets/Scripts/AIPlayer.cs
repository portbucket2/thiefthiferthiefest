﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class AIPlayer : MonoBehaviour
{
    public StageManager stageManager;
    public Vector3 distaceFromgate;
    public Vector3 laneOffset;
    public Animator animator;
    public float walkTime = 1.2f;

    private List<Vector3> lane;
    private int gateCount = 0;
    GameController gameController;
    bool aiWon = false;
    // Start is called before the first frame update


    private readonly string IDLE = "idle";
    private readonly string WALK = "walk";
    private readonly string VICTORY = "victory";
    private readonly string LOSE = "lose";

    void Start()
    {
        gameController = GameController.GetController();
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void InitPlayer(List<Vector3> _lane)
    {
        gateCount = 0;
        lane = new List<Vector3>();
        lane = _lane;
    }
    public void MoveToNext()
    {
        if (gateCount < lane.Count - 1)
        {
            transform.DOLocalMove((lane[gateCount] + laneOffset) - distaceFromgate, walkTime).SetEase(Ease.Linear).OnComplete(CallNextPuzzle);
        }
        else
        {
            Debug.Log("stage complete for AI");
            //transform.DOLocalMove(transform.localPosition + distaceFromgate * 20f, walkTime).SetEase(Ease.Linear).OnComplete(StageComplete); 
            transform.DOLocalMove((lane[gateCount] + laneOffset), walkTime).SetEase(Ease.Linear).OnComplete(StageComplete);
        }
        animator.SetTrigger(WALK);
    }
    void CallNextPuzzle()
    {
        StopWalking();
        StartCoroutine(WaitForFakePuzzle());
    }
    public void OnPuzzleComplete()
    {
        //puzzle panel off
        // move to next gate
        MoveToNext();
    }
    void StageComplete()
    {
        if (aiWon)
            Dance();
        else
            StopWalking();

    }
    void StopWalking()
    {
        animator.SetTrigger(IDLE);
    }
    public void Dance()
    {
        animator.SetTrigger(VICTORY);
    }
    public void Lose()
    {
        animator.SetTrigger(LOSE);
    }
    IEnumerator WaitForFakePuzzle()
    {
        gateCount++;
        float rand = Random.Range(7f, 12f);
        yield return new WaitForSeconds(rand);
        MoveToNext();
    }
    public void AIWon()
    {
        aiWon = true;
    }
}
