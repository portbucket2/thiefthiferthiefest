﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PipeBoundary : MonoBehaviour
{
    [SerializeField] PuzzlePipeRotate puzzlePipeRotate;
    private readonly string METABALL = "Metaball_liquid";

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag(METABALL))
        {
            puzzlePipeRotate.AddPiece();
            collision.gameObject.GetComponent<Rigidbody>().isKinematic = true;
            collision.gameObject.SetActive(false);
        }
    }
}
