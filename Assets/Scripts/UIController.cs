﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIController : MonoBehaviour
{
    
    public StageManager stageManager;

    public Button levelStartButton;
    [SerializeField] GameObject topPanel;
    [SerializeField] GameObject nameImage;
    [SerializeField] GameObject gameplayPanel;
    [SerializeField] GameObject levelCompletepanelWon;
    [SerializeField] GameObject levelCompletepanelLost;
    [SerializeField] Slider progressBar;
    [SerializeField] TextMeshProUGUI progressBarText;
    [SerializeField] Button nextButton;
    [SerializeField] Button resetButton;
    [SerializeField] Button resetButton2;

    GameManager gameManager;

    private void Awake()
    {
        
    }
    void Start()
    {
        gameManager = GameManager.GetManager();

        levelStartButton.onClick.AddListener(delegate {
            StartLevel();
	    });
        nextButton.onClick.AddListener(delegate {
            gameManager.GotoNextStage();
        });
        resetButton.onClick.AddListener(delegate {
            gameManager.ResetStage();
        });
        resetButton2.onClick.AddListener(delegate {
            gameManager.ResetStage();
        });
    }

    void StartLevel()
    {
        StartGameplayState();
    }
    /* states
     * pre gameplay start
     * start gameplay
     * 
     * loop start
     * running
     * puzzling
     * puzzle complete
     * loop end
     * 
     * reached finish line

    */

    void PregameplayStartState()
    {

    }
    public void StartGameplayState() {
        stageManager.StartLevel();
        levelStartButton.gameObject.SetActive(false);
        progressBar.gameObject.SetActive(true);
        nameImage.SetActive(false);
        //gameplayPanel.SetActive(true);
        levelCompletepanelWon.SetActive(false);
    }
    void RunningState() { }
    public void PuzzleStartState() {
        progressBar.gameObject.SetActive(false);
        //gameplayPanel.SetActive(false);
    }
    public void PuzzleCompleteState() {
        progressBar.gameObject.SetActive(true);
        //gameplayPanel.SetActive(true);
    }
    public void ReachedFinishState(bool playerWon) {
        if (playerWon)
        {
            progressBar.gameObject.SetActive(false);
            //gameplayPanel.SetActive(false);
            levelCompletepanelWon.SetActive(true);
        }
        else
        {
            progressBar.gameObject.SetActive(false);
            //gameplayPanel.SetActive(false);
            levelCompletepanelLost.SetActive(true);
        }
    }
    public void UpdateProgressbar(float _val)
    {
        float val = _val;
        if (val < 0)
            val = 0;
        else if (val > 1)
            val = 1;

        progressBar.value = val;
        progressBarText.text = (int)(val*100) + "%";
    }

}
