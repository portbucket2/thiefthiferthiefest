﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WireDetail : MonoBehaviour
{

    [SerializeField] WireColor wireColor;
    [SerializeField] Color currentColor;

    LineRenderer lineRenderer;
    Transform wireHead;
    private bool isMoving = false;

    void Start()
    {
        if (GetComponent<LineRenderer>())
        {
            lineRenderer = GetComponent<LineRenderer>();
        }
        if (transform.childCount>0)
        {
            wireHead = transform.GetChild(0);
        }

        SetColor(wireColor);
    }
    private void Update()
    {
        if (isMoving)
        {
            lineRenderer.SetPosition(1,wireHead.localPosition);
        }
    }
    public void SetColor(WireColor _color)
    {
        if (_color == WireColor.RED)
        {
            currentColor = Color.red;
        }
        else if(_color == WireColor.BLUE)
        {
            currentColor = Color.blue;
        }
        else if (_color == WireColor.GREEN)
        {
            currentColor = Color.green;
        }


        GetComponent<MeshRenderer>().material.SetColor("_Color", currentColor);
        if (lineRenderer)
        {
            lineRenderer.material.SetColor("_Color", currentColor);
        }

    }
    public Color getCurrentColor()
    {
        return currentColor;
    }

    public void IsMoving(bool _isMoving)
    {
        isMoving = _isMoving;
    }
    public void ResetWire()
    {
        if (lineRenderer)
        {
            lineRenderer.SetPosition(1, Vector3.zero);
        }
        if (transform.childCount > 0)
        {
            wireHead.localPosition = Vector3.zero;
        }
    }
    
}

public enum WireColor
{
    RED,
    BLUE,
    GREEN
}