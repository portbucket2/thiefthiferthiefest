﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class PuzzleShapeMatching : PuzzleGroup 
{
    public Camera puzzleCamera;
    [SerializeField] PuzzleManager puzzleManager;
    [SerializeField] ParticleSystem burstLight;

    [SerializeField] float dragSpeed = 10f;
    [SerializeField] float snapDistance = 0.5f;
    //[SerializeField] int wiresmached = 0;
    //[SerializeField] Color grabColor;
    [SerializeField] Transform[] pieces;
    private Vector3[] piecesStartLocation;

    [Header("for debug")]
    [SerializeField] private Transform grabObject;
    private readonly string WireHead = "WireHead";
    //private readonly string WireEnd = "WireEnd";
    private readonly string PuzzleGround = "PuzzleGround";

    [SerializeField] Vector3 floatingOffset = new Vector3(0f, 1f, 1f);
    [SerializeField] private bool isGrabbing = true;
    private Vector3 particleOffset = new Vector3(0, 1f, 0f);
    // Start is called before the first frame update
    void Start()
    {
        puzzleManager = transform.GetComponentInParent<PuzzleManager>();
        puzzleCamera = puzzleManager.puzzleCamera;
        burstLight = puzzleManager.burstLight;

        SetStartLocation();
    }
    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            isGrabbing = true;
            Ray ray = puzzleCamera.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, 100))
            {
                Debug.DrawLine(ray.origin, hit.point);
                if (hit.transform.CompareTag(WireHead))
                {
                    grabObject = hit.transform;
                }
                else
                {
                    grabObject = null;
                }
            }
        }
        if (Input.GetMouseButton(0))
        {
            if (grabObject && isGrabbing)
            {
                Ray ray = puzzleCamera.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;

                if (Physics.Raycast(ray, out hit, 100))
                {
                    Debug.DrawLine(ray.origin, hit.point);
                    if (hit.transform.CompareTag(PuzzleGround))
                    {
                        grabObject.position = Vector3.Lerp(grabObject.position, hit.point + floatingOffset, Time.deltaTime * dragSpeed);
                        grabObject.GetComponent<MeshCollider>().enabled = false;
                        CheckForShapeMatch();
                    }
                }
            }
        }
        if (Input.GetMouseButtonUp(0))
        {
            isGrabbing = false;
            if (grabObject)
            {
                grabObject.GetComponent<MeshCollider>().enabled = true;
                grabObject = null;
            }
            
        }
    }

    void CheckForShapeMatch()
    {
        Vector3 tempPos = new Vector3(grabObject.localPosition.x, 0f, grabObject.localPosition.z);
        float dis = Vector3.Distance(Vector3.zero, tempPos);
        if (dis < snapDistance)
        {
            //matched
            //Debug.Log("Matched!!");
            grabObject.DOLocalMove(Vector3.zero, 0.2f).SetEase(Ease.Flash).OnComplete(OnMatched);
            isGrabbing = false;
            burstLight.transform.position = grabObject.position + particleOffset;
            burstLight.Play();
        }
    }

    void OnMatched()
    {
        //Debug.Log("Object in position!!");
        grabObject.localPosition = Vector3.zero;
        grabObject.GetComponent<MeshCollider>().enabled = false;
        grabObject = null;
        PuzzleCompleteCheck();
    }

    void PuzzleCompleteCheck()
    {
        int puzzleComplete = 0;
        int max = pieces.Length;
        for (int i = 0; i < max; i++)
        {
            if (pieces[i].localPosition == Vector3.zero)
            {
                puzzleComplete++;
                //Debug.Log("puzzle complete true for:" + i);
            }
        }

        if (puzzleComplete >= max) 
            StartCoroutine(PuzzleCompleteRoutine());

    }
    IEnumerator PuzzleCompleteRoutine()
    {
        yield return new WaitForSeconds(0.5f);
        puzzleManager.PuzzleCompleted();
        
        yield return new WaitForSeconds(0.5f);
        ResetPuzzle();
        gameObject.SetActive(false);
    }

    public void ResetPuzzle()
    {
        int max = pieces.Length;
        for (int i = 0; i < max; i++)
        {
            pieces[i].localPosition = piecesStartLocation[i];
            pieces[i].GetComponent<MeshCollider>().enabled = true;
        }
    }

    void SetStartLocation()
    {
        int max = pieces.Length;
        piecesStartLocation = new Vector3[max];
        for (int i = 0; i < max; i++)
        {
            piecesStartLocation[i] = pieces[i].localPosition;
        }
    }
}
