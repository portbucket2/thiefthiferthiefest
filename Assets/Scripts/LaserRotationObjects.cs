﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserRotationObjects : MonoBehaviour
{
    [SerializeField] bool isAvailable = true;
    [Header("Auto filled!!")]
    [SerializeField] PuzzleLaserMatch puzzleLaserMatch;
    [SerializeField] Camera puzzleCamera;

    Vector3 rotation;
    void Start()
    {
        rotation = new Vector3(0f, 30f, 0f);
        puzzleLaserMatch = transform.parent.GetComponent<PuzzleLaserMatch>();
        puzzleCamera = puzzleLaserMatch.GetCamera();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void RotateOnce()
    {

        this.transform.localEulerAngles = transform.localEulerAngles + rotation;
    }
}
