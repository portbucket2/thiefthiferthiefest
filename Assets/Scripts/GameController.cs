﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public static GameController gameController;
    public StageManager stageManager;
    public UIController uiController;
    public PlayerController playerController;
    public PuzzleManager puzzleManager;
    
    public ParticleSystem FinishLineParticle;

    Camera puzzleCamera;

    GameManager gameManager;

    WaitForSeconds WAITTWO = new WaitForSeconds(3f);

    bool playerWon = false;

    private void Awake()
    {
        gameController = this;
    }

    void Start()
    {
        gameManager = GameManager.GetManager();
        playerWon = false;
        puzzleCamera = puzzleManager.puzzleCamera;
    }

    public static GameController GetController()
    {
        return gameController;
    }

    public void StartPuzzle()
    {
        StatePuzzleStart();
        puzzleManager.StartNextPuzzle();
        puzzleCamera.gameObject.SetActive(true);
    }

    public void CompletePuzzle()
    {
        StatePuzzleComplete();
        puzzleCamera.gameObject.SetActive(false);
        playerController.MoveToNext();
    }
    public void LevelComplete()
    {
        if (playerWon) // player won
        {
            Debug.Log("winner");
            FinishLineParticle.Play();
            stageManager.GetGateEffects().SetActive(false);
            
        }
        else
        {
            Debug.Log("loser");
            // show lost UI
        }
        stageManager.StageEndAnimation(playerWon);
        StateFinishLine();
    }
    public FinishLinePedestal GetPedestal()
    {
        return stageManager.GetPedestal();
    }
    public void PlayerWon(bool won)
    {
        playerWon = won;
    }

    public void StatePuzzleStart()
    {
        uiController.PuzzleStartState();
    }
    public void StatePuzzleComplete()
    {
        uiController.PuzzleCompleteState();
    }
    public void StateFinishLine()
    {
        uiController.ReachedFinishState(playerWon);
    }
    public void UpdateProgressBar(float val)
    {
        uiController.UpdateProgressbar(val);
    }
}
