﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ObstacleRemover : MonoBehaviour
{
    bool checkComplete = false;
    Transform mother;

    void Start()
    {
        mother = transform.parent;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!checkComplete)
        {
            if (other.transform.CompareTag("Player"))
            {
                //Debug.Log("player won");
                Shirink();                
            }
            else if (other.transform.CompareTag("Enemy"))
            {
                //Debug.Log("enemy won");
                Shirink();
            }
        }
    }
    void Shirink()
    {
        mother.DOScale(Vector3.zero, 0.5f).SetEase(Ease.Flash);
    }
}
