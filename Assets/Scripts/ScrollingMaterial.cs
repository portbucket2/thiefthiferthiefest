﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollingMaterial : MonoBehaviour
{
    [SerializeField] bool isScrolling = true;
    [SerializeField] bool isLine = true;
    [SerializeField] Material material;
    [Range(-0.5f,0f)]
    [SerializeField] float speed = -0.005f;

    float val = 0;
    Vector2 offset = Vector2.zero;

    void Start()
    {
        if(isLine)
            material = GetComponent<LineRenderer>().material;
        else
            material = GetComponent<MeshRenderer>().material;
    }

    // Update is called once per frame
    void Update()
    {
        if (isScrolling)
        {
            val += speed;
            offset = new Vector2(val,0f);
            material.SetTextureOffset("_MainTex", offset);
            if (val < -0.99f)
                val = 0;
        }
        
    }
}
