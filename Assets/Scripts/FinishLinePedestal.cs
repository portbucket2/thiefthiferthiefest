﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinishLinePedestal : MonoBehaviour
{
    [SerializeField] Material goldMat;

    [SerializeField] Renderer negetive;
    [SerializeField] Renderer zero;
    [SerializeField] Renderer positive;

    [SerializeField] GameObject negetiveCoin;
    [SerializeField] GameObject zeroCoin;
    [SerializeField] GameObject positiveCoin;

    [SerializeField] ParticleSystem coinParticle;

    bool checkComplete = false;
    GameController gameController;

    void Start()
    {
        gameController = GameController.GetController();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!checkComplete)
        {
            if (other.transform.CompareTag("Player"))
            {
                Debug.Log("player won");
                checkComplete = true;
                GoldCheck(other.transform.position.x);
                // player won
                gameController.PlayerWon(true);
                coinParticle.Play();
            }
            else if (other.transform.CompareTag("Enemy"))
            {
                Debug.Log("enemy won");
                checkComplete = true;
                GoldCheck(other.transform.position.x);
                //player lost
                gameController.PlayerWon(false);
                other.GetComponent<AIPlayer>().AIWon();
            }
            negetiveCoin.SetActive(false);
            zeroCoin.SetActive(false);
            positiveCoin.SetActive(false);
        }
    }

    void GoldCheck(float val)
    {
        if (val<0){ negetive.material = goldMat; }
        else if (val == 0){ zero.material = goldMat; }
        else if (val > 0) { positive.material = goldMat; }
    }
}
