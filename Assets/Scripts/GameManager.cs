﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager gameManager;
    


    [Header("For Debugging")]
    public int levelcount = 0;
    public int totalScenes = 0;


    private void Awake()
    {
        gameManager = this;
        DontDestroyOnLoad(this.gameObject);
    }
    void Start()
    {
        totalScenes = SceneManager.sceneCountInBuildSettings;
        GotoNextStage();
    }

    public static GameManager GetManager()
    {
        return gameManager;
    }
    public void GotoNextStage()
    {
        if (levelcount < totalScenes - 1) 
            levelcount++;
        else
            levelcount = 0;        

        SceneManager.LoadScene(levelcount);
    }
    public void ResetStage()
    {
        SceneManager.LoadScene(levelcount);
    }
}
