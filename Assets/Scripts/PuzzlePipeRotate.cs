﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzlePipeRotate : PuzzleGroup
{
    public Camera puzzleCamera;

    [SerializeField] PuzzleManager puzzleManager;
    [SerializeField] float dragSpeed = 10f;
    
    [SerializeField] Transform graoundBoundary;
    [SerializeField] Transform watertank;
    [SerializeField] Transform mainPipe;
    [SerializeField] Transform roundPipe;
    [SerializeField] Transform squarePipe;

    [SerializeField] bool isRound = true;

    [Header("Auto fill !!")]
    [SerializeField] Transform[] pieces;
    Vector3[] piecesLocation;
    Rigidbody[] piecesRB;

    private readonly string WireHead = "WireHead";
    float rotX;
    int totalPieces;
    [SerializeField] int counter = 0;
    Vector3 startPosition;
    // Start is called before the first frame update
    void Start()
    {
        puzzleManager = transform.GetComponentInParent<PuzzleManager>();
        puzzleCamera = puzzleManager.puzzleCamera;

        if (isRound)
        {
            mainPipe = roundPipe;
            roundPipe.gameObject.SetActive(true);
            squarePipe.gameObject.SetActive(false);
        }
        else
        {
            mainPipe = squarePipe;
            roundPipe.gameObject.SetActive(false);
            squarePipe.gameObject.SetActive(true);
        }

        int total = watertank.childCount;
        pieces = new Transform[total];
        for (int i = 0; i < total; i++)
        {
            pieces[i] = watertank.GetChild(i);
        }

        totalPieces = pieces.Length;
        piecesLocation = new Vector3[totalPieces];
        for (int i = 0; i < totalPieces; i++)
        {
            piecesLocation[i] = pieces[i].localPosition;
        }

        piecesRB = new Rigidbody[totalPieces];
        for (int i = 0; i < totalPieces; i++)
        {
            piecesRB[i] = pieces[i].GetComponent<Rigidbody>();
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            startPosition = Input.mousePosition;
        }
        if (Input.GetMouseButton(0))
        {
            rotX = Input.GetAxis("Mouse X") * dragSpeed * Mathf.Deg2Rad;
            mainPipe.Rotate(Vector3.forward, rotX, Space.World);
            //if (rotX > 0.01f)
            //{
            //    // make the pieces rigidbody isKinematic false
            //    for (int i = 0; i < totalPieces; i++)
            //    {
            //        piecesRB[i].isKinematic = true;
            //    }
            //}
            //else
            //{
            //    for (int i = 0; i < totalPieces; i++)
            //    {
            //        piecesRB[i].isKinematic = false;
            //    }
            //}
        }
        if (Input.GetMouseButtonUp(0))
        {
            
        }
        
    }
    public void AddPiece() {
        counter++;
        PuzzleCompleteCheck();
    }







    void PuzzleCompleteCheck()
    {
        // if bucket is full complete puzzle


        if (counter > totalPieces * 0.80f) 
            StartCoroutine(PuzzleCompleteRoutine());

    }
    IEnumerator PuzzleCompleteRoutine()
    {
        yield return new WaitForSeconds(0.5f);
        puzzleManager.PuzzleCompleted();

        gameObject.SetActive(false);
        yield return new WaitForSeconds(0.5f);
        ResetPuzzle();
    }

    public void ResetPuzzle()
    {
        counter = 0;
        for (int i = 0; i < totalPieces; i++)
        {
            pieces[i].localPosition = piecesLocation[i];
            pieces[i].GetComponent<Rigidbody>().isKinematic = false;
            pieces[i].gameObject.SetActive(true);
            mainPipe.localEulerAngles = new Vector3(0f, 90f, -90f);
        }
    }

}
