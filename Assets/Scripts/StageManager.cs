﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class StageManager : MonoBehaviour
{
    public int column = 1;
    public int row = 4;
    public float rowDistance = 2f;

    [Header("Prefabs")]
    public Material levelMaterial;
    public Transform inSceneWalkingPath;
    public GameObject walkingPath;
    public GameObject ghostObstacle;
    public GameObject mirrorObstacle;
    public GameObject dragonObstacle;
    public GameObject holeObstacle;
    public GameObject finishline;
    public GameObject stageGate;
    public GameObject piller;
    public ParticleSystem finishlineParticle;


    //public GameObject simpleObstacle;


    public PlayerController player;
    public List<AIPlayer> enemyPlayers;
    public List<GameObject> obstacles;

    List<Vector3> lane;
    List<GameObject> obstaclePrefabs;

    Vector3 gateOffset = new Vector3(0f,20f,20f);
    Vector3 roadOffset = new Vector3(0f,0f,20f);

    private FinishLinePedestal pedestal;
    GameObject gateEffects;

    private void Awake()
    {
        obstaclePrefabs = new List<GameObject>();
        //obstaclePrefabs.Add(ghostObstacle);
        //obstaclePrefabs.Add(mirrorObstacle);
        //obstaclePrefabs.Add(dragonObstacle);
        //obstaclePrefabs.Add(holeObstacle);
        //obstaclePrefabs.Add(finishline);
        int max = obstacles.Count;
        for (int i = 0; i < max; i++)
        {
            obstaclePrefabs.Add(obstacles[i]);
        }
    }

    void Start()
    {
        CreateStage();
        ChangePathMat(inSceneWalkingPath);
    }

    public FinishLinePedestal GetPedestal()
    {
        return pedestal;
    }
    void CreateStage()
    {
        Vector3 startPos = Vector3.zero;
        lane = new List<Vector3>();
        for (int i = 0; i < row; i++)
        {
            lane.Add(new Vector3(0, 0,rowDistance + i * rowDistance));
        }

        for (int i = 0; i < row; i++)
        {
            if (i == row - 1)
            {
                GameObject finish = Instantiate(finishline, lane[i], finishline.transform.rotation, transform);
                pedestal = finish.GetComponent<FinishLinePedestal>();
                GameObject gate = Instantiate(stageGate, lane[i] + gateOffset, stageGate.transform.rotation, transform);
                GameObject road1 = Instantiate(walkingPath, lane[i] + roadOffset, walkingPath.transform.rotation, transform);
                GameObject road2 = Instantiate(walkingPath, lane[i] + roadOffset * 2f, walkingPath.transform.rotation, transform);
                GameObject road3 = Instantiate(walkingPath, lane[i] + roadOffset * 3f, walkingPath.transform.rotation, transform);
                finishlineParticle.transform.position = lane[i] + gateOffset;

                ChangePathMat(gate.transform);
                ChangePathMat(road1.transform);
                ChangePathMat(road2.transform);
                ChangePathMat(road3.transform);
                gateEffects = gate.transform.GetChild(0).gameObject;

            }
            else
            {//instantiate obstacle
                GameObject gg = Instantiate(obstaclePrefabs[i], lane[i], obstaclePrefabs[i].transform.rotation, transform);
            }
            //instantiate walking path
            GameObject gs = Instantiate(walkingPath,lane[i], walkingPath.transform.rotation, transform);
            ChangePathMat(gs.transform);
            SpwanPiller(lane[i]);
            
            //IconManager.SetIcon(gg, IconManager.LabelIcon.Blue);
        }

        
    }
    public void StartLevel()
    {
        player.InitPlayer(lane);
        player.MoveToNext();

        int max = enemyPlayers.Count;
        for (int i = 0; i < max; i++)
        {
            enemyPlayers[i].InitPlayer(lane);
            enemyPlayers[i].MoveToNext();
        }
    }
    public GameObject GetGateEffects()
    {
        return gateEffects;
    }

    void SpwanPiller(Vector3 pos)
    {
        float scale = Random.Range(500f, 1000f);
        float scale2 = Random.Range(500f, 1000f);
        Vector3 loc = new Vector3(Random.Range(-30f, -10f), Random.Range(-80f, -70f), pos.z + Random.Range(-4f, 4f));
        Vector3 loc2 = new Vector3(Random.Range(-40f, -20f), Random.Range(-80f, -70f), pos.z + Random.Range(-4f, 4f));

        GameObject pil = Instantiate(piller, pos + loc , piller.transform.rotation, transform);
        pil.transform.localScale = new Vector3(Random.Range(2000f, 2500f), scale, scale);

        GameObject pil2 = Instantiate(piller, pos + loc2, piller.transform.rotation, transform);
        pil2.transform.localScale = new Vector3(Random.Range(2000f, 2500f), scale2, scale2);
    }
    void ChangePathMat(Transform gg)
    {
        int max = gg.childCount;
        for (int i = 0; i < max; i++)
        {
            if (gg.GetChild(i).GetComponent<MeshRenderer>() )
            {
                gg.GetChild(i).GetComponent<MeshRenderer>().material = levelMaterial;
            }
        }
    }

    public void StageEndAnimation(bool playerWon)
    {
        if (playerWon)
        {
            player.Dance();
        }
        else
        {
            player.Lose();
        }
    }
}
