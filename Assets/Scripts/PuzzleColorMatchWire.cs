﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class PuzzleColorMatchWire : MonoBehaviour
{
    public Camera puzzleCamera;

    [SerializeField] PuzzleManager puzzleManager;
    [SerializeField] int wiresmached = 0;
    [SerializeField] Color grabColor;
    [SerializeField] WireDetail[] wires;
    private float speed = 10f;
    private Transform grabObject;
    private readonly string WireHead = "WireHead";
    private readonly string WireEnd = "WireEnd";
    private readonly string PuzzleGround = "PuzzleGround";


    private readonly string SET = "set";
    private readonly string VALUE = "value";
    Animator animator;
    private Vector3 offset = new Vector3(0,0.5f,0);
    void Start()
    {
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = puzzleCamera.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, 100))
            {
                Debug.DrawLine(ray.origin, hit.point);
                if (hit.transform.CompareTag(WireHead))
                {
                    grabObject = hit.transform;
                    grabObject.GetComponentInParent<WireDetail>().IsMoving(true);
                    grabColor = grabObject.GetComponentInParent<WireDetail>().getCurrentColor();
                }
                else
                {
                    grabObject = null;
                }
            }
        }
        if (Input.GetMouseButton(0))
        {
            if (grabObject)
            {
                Ray ray = puzzleCamera.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;
                
                if (Physics.Raycast(ray, out hit, 100))
                {
                    Debug.DrawLine(ray.origin, hit.point);
                    if (hit.transform.CompareTag(PuzzleGround))
                    {
                        grabObject.position = Vector3.Lerp(grabObject.position, hit.point + offset, Time.deltaTime * speed);
                    }
                }
            }
        }
        if (Input.GetMouseButtonUp(0))
        {
            Ray ray = puzzleCamera.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, 100))
            {
                Debug.DrawLine(ray.origin, hit.point);
                if (hit.transform.CompareTag(WireEnd))
                {
                    Color color =  hit.transform.GetComponent<WireDetail>().getCurrentColor();
                    
                    if (color ==  grabColor)
                    {
                        Debug.Log("color match!!");
                        grabObject.DOLocalMove(grabObject.parent.transform.InverseTransformPoint(hit.transform.position), 0.2f).SetEase(Ease.Flash).OnComplete(OnCompleteWireConnected);
                        
                    }
                    else
                    {
                        ResetGrabObject();
                    }

                }
                else
                {
                    ResetGrabObject();
                }
            }
        }
    }


    void ResetGrabObject()
    {
        grabObject.DOLocalMove(Vector3.zero, 0.5f).SetEase(Ease.Linear).OnComplete(OnCompleteResetMove);
    }
    void OnCompleteResetMove()
    {
        grabObject.GetComponentInParent<WireDetail>().IsMoving(false);
        grabObject = null;
    }

    void OnCompleteWireConnected()
    {
        grabObject.GetComponentInParent<WireDetail>().IsMoving(false);
        wiresmached++;
        PuzzleCompleteCheck();
    }

    void PuzzleCompleteCheck()
    {
        if (wiresmached>=3)
        {
            Debug.Log("Puzzle complete!!!");
            StartCoroutine(PuzzleCompleteRoutine());
        }
    }
    IEnumerator PuzzleCompleteRoutine()
    {
        yield return new WaitForSeconds(0.5f);
        puzzleManager.PuzzleCompleted();

        yield return new WaitForSeconds(0.5f);
        ResetPuzzle();
    }

    public void ResetPuzzle()
    {
        wiresmached = 0;
        int max = wires.Length;
        for (int i = 0; i < max; i++)
        {
            wires[i].ResetWire();
        }
        int val = Random.Range(0,3);
        animator.SetFloat(VALUE,val);
        animator.SetTrigger(SET);
    }
}
